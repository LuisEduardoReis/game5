import { TEXTURES }from './graphics'
import { TILES } from './tiles'
import { getPixelValue } from './graphics'
import { Spawner } from './entities/spawner'
import { Pickup } from './entities/pickup'
import { newPoint, setUniformTexture, pointDistanceSquared2d } from './util'
import { createLevelModel } from './models'
import { doEntityCollisions, doLevelCollisions } from './collisions'
import { g3d, player } from '.'
import { theShader } from './graphics'

export class Level {

    constructor() {
        this.t = 0
        this.entities = []
        this.tiles = []
        this.xsize = TEXTURES.map1.height
        this.ysize = 6
        this.zsize = TEXTURES.map1.height

        let nTiles = this.xsize * this.ysize * this.zsize;
        for(let i = 0; i < nTiles; i++) {
            this.tiles.push(TILES.EMPTY)
        }

        TEXTURES.map1.loadPixels()
        for(let y = 0; y < this.ysize; y++)
        for(let x = 0; x < this.xsize; x++)
        for(let z = 0; z < this.zsize; z++) {
            let i = 4*(x + z * this.xsize * this.ysize + y * this.xsize)
            this.processMapPixel(getPixelValue(TEXTURES.map1.pixels, i), x,this.ysize - 1 - y,z)
        }

        this.generateModel()
    }

    generateModel() {
        const create_model_time = window.performance.now()
        this.model = createLevelModel(this)
        console.log("create_model_time", Math.floor(window.performance.now() - create_model_time) + " ms")
    }
    
    update(delta) {
        this.t += delta

        this.entities.forEach(e => e.preupdate(delta))
        this.entities.forEach(e => e.update(delta))

        doEntityCollisions(this)
        doLevelCollisions(this, delta)

        if (this.entities.some(e => e.remove))
            this.entities = this.entities.filter(e => !e.remove)
    }

    draw() {
        g3d.noStroke()

        setUniformTexture(theShader, TEXTURES.tilemap)
        g3d.model(this.model)
        /*
        for(let z = 0; z < this.zsize; z++)
        for(let y = 0; y < this.ysize; y++)
        for(let x = 0; x < this.xsize; x++) {
            let t = this.getTile(x,y,z)
            if (t == TILES.EMPTY) continue

            pushMatrices(g3d)
            g3d.translate(x, y, z)
            
            if (t.texture) setUniformTexture(theShader, t.texture)
            else setUniformTexture(theShader, TEXTURES.test)

            if (t.type.solid_block) {
                g3d.translate(0.5,0.5,0.5)
                g3d.scale(0.5,0.5,0.5)
                g3d.model(MODELS.BLOCK)
            }
            if (t.type == TILE_TYPES.RAMP) {
                g3d.translate(0.5,0,0.5)
                g3d.rotateY(t.model_rotation_y)
                g3d.translate(-0.5,0,-0.5)
                g3d.model(MODELS.RAMP)
            }
            popMatrices(g3d)
        }*/
        // Test hitscan sphere
        /*let result = doWeaponHitScan(this, player.x, player.y - player.radius, player.z, player.lookDirectionHorizontal, player.lookDirectionVertical)
        if (result.hit) {
            push()
            translate(result.x, result.y, result.z)
            fill(result.entityHit ? "#ff0000" : "#ffffff")
            sphere(0.05)
            pop()
        }*/

        this.entities.forEach(e => e.distanceToPlayer = pointDistanceSquared2d(e.x,e.z, player.x,player.z))
        this.entities.sort((a,b) => b.distanceToPlayer - a.distanceToPlayer)
        this.entities.forEach(e => e.draw())
    }

    addEntity(e) {
        e.level = this
        this.entities.push(e)
        return e
    }

    getTile(x,y,z) {
        if (y >= this.ysize) return TILES.TEST_WALL
        if (y < 0) return TILES.EMPTY
        if (x < 0 || x >= this.xsize) return TILES.EMPTY
        if (z < 0 || z >= this.zsize) return TILES.EMPTY
        let i = z * this.xsize * this.ysize + y * this.xsize + x

        return this.tiles[i]
    }

    setTile(x,y,z, t) {
        let i = z * this.xsize * this.ysize + y * this.xsize + x
        this.tiles[i] = t
    }

    processMapPixel(color, x,y,z) {
        let t = colorToTile(color)
        this.setTile(x,y,z,t)

        let e = colorToSpawnableEntity(color,x,y,z)
        if (e) this.addEntity(e)

        processColorCommands(this, color,x,y,z)
    }
}

function colorToTile(color) {
    if (color == 0xFFFFFF) return TILES.EMPTY
    if (color == 0x000000) return TILES.TEST_WALL
    if (color == 0x7F3300) return TILES.DIRT
    if (color == 0x808080) return TILES.METAL_BLOCK
    if (color == 0x7F0000) return TILES.BRICK_BLOCK
    if (color == 0x7F6B55) return TILES.RUST_BLOCK
    if (color == 0xA59F81) return TILES.TILES
    if (color == 0xA0A0A0) return TILES.SMALL_TILES

    if ((color & 0xFFFF00) == 0x008000) {
        if ((color & 0xFF) == 0) return TILES.RAMP_WEST
        if ((color & 0xFF) == 64) return TILES.RAMP_NORTH
        if ((color & 0xFF) == 128) return TILES.RAMP_EAST
        if ((color & 0xFF) == 192) return TILES.RAMP_SOUTH
    }

    return TILES.EMPTY
}

function colorToSpawnableEntity(color,x,y,z) {
    if (color == 0xFF0000) return new Spawner().setPos(x + 0.5, y + 0.75, z + 0.5)
    if (color == 0xFF8000) return new Pickup().setPos(x + 0.5, y + 0.75, z + 0.5)
    return null
}

function processColorCommands(level, color,x,y,z) {
    if (color == 0xFF00FF) {
        level.spawn = newPoint(x,y,z)
    }
}