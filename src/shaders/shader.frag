#ifdef GL_ES
precision mediump float;
#endif

// we need our texcoords for drawing textures
varying vec2 vTexCoord;
varying vec3 vNormal;
varying float vZ;

uniform sampler2D texture;
uniform bool flipTexU;
uniform bool flipTexV;

uniform bool useFill;
uniform vec4 fillColor;

void main() {
  vec2 uv = vTexCoord;
  if (!flipTexU) uv.x = 1.0 - uv.x;
  if (flipTexV) uv.y = 1.0 - uv.y;

  // we can access our image by using the GLSL shader function texture2D()
  // texture2D expects a sampler2D, and texture coordinates as it's input
  vec4 textureColor = texture2D(texture, uv);
  float shade = 0.75;

  if (useFill) {
    gl_FragColor = fillColor;
  } else if (vNormal.x > 0.0 || vNormal.z > 0.0 || vNormal.y < 0.0) {
    gl_FragColor = vec4(textureColor.r * shade, textureColor.g * shade, textureColor.b * shade, textureColor.a);
  } else {
    gl_FragColor = vec4(textureColor.r, textureColor.g, textureColor.b, textureColor.a);
  }
  float fog = max(0.1, 1.0 - (vZ / 10.0));
  gl_FragColor.r *= fog;
  gl_FragColor.g *= fog;
  gl_FragColor.b *= fog;
}