import { newPoint } from "./util";
import { CARDINAL_NEIGHBOORS, DIRECTION_TO_CARDINAL, DOWN } from "./util";

export function getNodeId(node) {
    return node.x + ',' + node.y + ',' + node.z
}

export function findPath(level, fromX,fromY,fromZ,  toX,toY,toZ) {
    fromX = Math.floor(fromX); fromY = Math.floor(fromY); fromZ = Math.floor(fromZ)
    toX = Math.floor(toX); toY = Math.floor(toY); toZ = Math.floor(toZ)

    while(!level.getTile(toX, toY+1, toZ).type.solid_block) toY++
    
    let searchHeap = []
    let cameFrom = {}
    let visited = {}

    searchHeap.push(newPoint(fromX, fromY, fromZ))
    let i = 0
    while(searchHeap.length > 0 && i++ < 1000) {
        let node = searchHeap.shift()
        let nodeId = getNodeId(node)
        visited[nodeId] = true

        if (node.x == toX && node.y == toY && node.z == toZ) break

        let checkNeighboor = (dx,dy,dz) => {
            let newNode = newPoint(node.x + dx, node.y + dy, node.z + dz)
            let newNodeId = getNodeId(newNode)

            if (visited[newNodeId]) return
            if (cameFrom[newNodeId]) return
            let tileAt = level.getTile(newNode.x, newNode.y, newNode.z)
            if(tileAt.type.solid_block) return
            if(tileAt.type.ramp) {
                let rampDir = DIRECTION_TO_CARDINAL[tileAt.ramp_direction]
                if (dy == 0 && !(rampDir.x == -dx && rampDir.z == -dz)) return
            }
            
            let tileBelow = level.getTile(newNode.x, newNode.y+1, newNode.z)
            if (tileBelow.type.ramp) checkNeighboor(dx,dy+1,dz)

            if (!tileBelow.type.solid_block) return
            
            searchHeap.push(newNode)
            cameFrom[newNodeId] = node
        }

        if(level.getTile(node.x, node.y, node.z).type.ramp) {
            let rampDir = DIRECTION_TO_CARDINAL[level.getTile(node.x, node.y, node.z).ramp_direction]
            checkNeighboor(-rampDir.x, -1, -rampDir.z)
        }
        if(level.getTile(node.x, node.y+1, node.z).type.ramp) checkNeighboor(DOWN.x ,DOWN.y, DOWN.z)        

        CARDINAL_NEIGHBOORS.forEach(d => checkNeighboor(d.x,d.y,d.z))
    }

    let path = []
    let reverseNode = cameFrom[toX + ',' + toY + ',' + toZ]
    while(reverseNode) {
        path.unshift(reverseNode)
        reverseNode = cameFrom[reverseNode.x + ',' + reverseNode.y + ',' + reverseNode.z]
    }

    return path
}