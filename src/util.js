import { theShader } from "./graphics"

export function newPoint(x,y,z) {
    return {x:x, y:y, z:z}
}

export const UP = newPoint(0,-1,0)
export const DOWN = newPoint(0,1,0)
export const WEST = newPoint(-1,0,0)
export const EAST = newPoint(1,0,0)
export const NORTH = newPoint(0,0,-1)
export const SOUTH = newPoint(0,0,1)
export const CARDINAL_NEIGHBOORS = [ WEST, EAST, NORTH, SOUTH ]

export const DIRECTION_TO_CARDINAL = {
    "+x": EAST,
    "-x": WEST,
    "+z": SOUTH,
    "-z": NORTH
}

export function clamp(x,a,b) {
    return Math.min(Math.max(x,a),b)
}

export function randomRange(a,b) {
    return a + Math.random() * (b-a)
}

export function clampAbsoluteValue(x, a,b) {
    return clamp(Math.abs(x), a,b) * Math.sign(x)
}

export function lineToPlane(p0x,p0y,p0z, p1x,p1y,p1z, p_cox,p_coy,p_coz, p_nox,p_noy,p_noz) {

    let ux = p1x - p0x
    let uy = p1y - p0y
    let uz = p1z - p0z    

    let dot = p_nox * ux + p_noy * uy + p_noz * uz

    if (Math.abs(dot) < 1e-10) return null

    let wx = p0x - p_cox
    let wy = p0y - p_coy
    let wz = p0z - p_coz

    let dot_w = p_nox * wx + p_noy * wy + p_noz * wz
    let fac = - dot_w / dot
    
    ux *= fac
    uy *= fac
    uz *= fac

    return {
        x: p0x + ux,
        y: p0y + uy,
        z: p0z + uz,
        distance: Math.sign(fac) * Math.sqrt(ux*ux + uy*uy + uz*uz),
    }
}

export function between(x,a,b) {
    return x >= a && x <= b
}

export function stepTo(a,b,x) {
    if (Math.abs(b - a) <= x) return b
    return a + x * Math.sign(b - a)
}

export function pointDistance2d(ax,ay,bx,by) {
    ax -= bx; ay -= by;
    return Math.sqrt(ax*ax + ay*ay)
}
export function pointDistance3d(ax,ay,az, bx,by,bz) {
    ax -= bx; ay -= by; az -= bz
    return Math.sqrt(ax*ax + ay*ay + az*az)
}
export function pointDistanceSquared2d(ax,ay,bx,by) {
    ax -= bx; ay -= by;
    return ax*ax + ay*ay
}
export function pointDistanceSquared3d(ax,ay,az, bx,by,bz) {
    ax -= bx; ay -= by; az -= bz
    return ax*ax + ay*ay + az*az
}

export function lookAnglesToVector(horizontal, vertical) {
    let r = {}
    r.x = Math.cos(-horizontal) * Math.cos(-vertical)
    r.z = Math.sin(-horizontal) * Math.cos(-vertical)
    r.y = Math.sin(-vertical)
    return r
}

let uMVMatrixStack = []
let uPMatrixStack = []
export function pushMatrices(graphics) {
    uMVMatrixStack.push(graphics._renderer.uMVMatrix.copy())
    uPMatrixStack.push(graphics._renderer.uPMatrix.copy())
}

export function popMatrices(graphics) {
    let MVMatrix = uMVMatrixStack.pop()
    let PMatrix = uPMatrixStack.pop()
    if (!MVMatrix || !PMatrix) console.log("popMatrices() called without corresponding push")
    else {
        graphics._renderer.uMVMatrix = MVMatrix
        graphics._renderer.uPMatrix = PMatrix
    }
}

export function setUniformTexture(shader, texture) {
    //if (shader.lastSetTexture != texture) {
        shader.setUniform("texture", texture)
        //shader.lastSetTexture = texture
    //}
}

export function setFill(rgba) {
    theShader.setUniform("useFill", true)
    theShader.setUniform("fillColor", rgba)
}
export function setNoFill() {
    theShader.setUniform("useFill", false)
}

export function p5ColorToVec4(c) {
    return color(c)._array
}