
export class TileType {
    constructor() {
        this.solid_block = false
        this.ramp = false
    }
}
export class Tile {   
    constructor(type) {
        
        this.type = type
        this.texture = null
        this.ramp_direction = null
        this.model_rotation_y = 0
        this.color = '#ff0000'
        this.u0 = 0; this.u1 = 0; this.v0 = 0; this.v1 = 0; 
    }

    setTextureUV(u,v) {
        this.u0 = u / 8; this.u1 = (u + 1) / 8
        this.v0 = v / 8; this.v1 = (v + 1) / 8
        return this
    }
    setColor(color) {
        this.color = color
        return this
    }
}

export const TILES = {}
export const TILE_TYPES = {}

export function setupTiles() {
    TILE_TYPES.EMPTY = new TileType()
    TILE_TYPES.EMPTY.solid_block = false

    TILE_TYPES.BLOCK = new TileType()
    TILE_TYPES.BLOCK.solid_block = true
    

    TILE_TYPES.RAMP = new TileType()
    TILE_TYPES.RAMP.ramp = true

    
    TILES.EMPTY = new Tile(TILE_TYPES.EMPTY)
    
    TILES.TEST_WALL = new Tile(TILE_TYPES.BLOCK).setTextureUV(1,0).setColor("#b7b7b7")
    TILES.DIRT = new Tile(TILE_TYPES.BLOCK).setTextureUV(5,0).setColor("#604023")
    TILES.METAL_BLOCK = new Tile(TILE_TYPES.BLOCK).setTextureUV(2,0).setColor("#808080")
    TILES.RUST_BLOCK = new Tile(TILE_TYPES.BLOCK).setTextureUV(3,0).setColor("#5B4F43")
    TILES.BRICK_BLOCK = new Tile(TILE_TYPES.BLOCK).setTextureUV(4,0).setColor("#83433C")
    TILES.TILES = new Tile(TILE_TYPES.BLOCK).setTextureUV(6,0).setColor("#A59F81")
    TILES.SMALL_TILES = new Tile(TILE_TYPES.BLOCK).setTextureUV(7,0).setColor("#A0A0A0")


    TILES.RAMP_EAST = new Tile(TILE_TYPES.RAMP).setTextureUV(2,0).setColor("#808080")
    TILES.RAMP_EAST.ramp_direction = "+x"
    TILES.RAMP_EAST.model_rotation_y = Math.PI / 2

    TILES.RAMP_WEST = new Tile(TILE_TYPES.RAMP).setTextureUV(2,0).setColor("#808080")
    TILES.RAMP_WEST.ramp_direction = "-x"
    TILES.RAMP_WEST.model_rotation_y = - Math.PI / 2

    TILES.RAMP_NORTH = new Tile(TILE_TYPES.RAMP).setTextureUV(2,0).setColor("#808080")
    TILES.RAMP_NORTH.ramp_direction =  "-z"
    TILES.RAMP_NORTH.model_rotation_y = Math.PI

    TILES.RAMP_SOUTH= new Tile(TILE_TYPES.RAMP).setTextureUV(2,0).setColor("#808080")
    TILES.RAMP_SOUTH.ramp_direction =  "+z"
}


