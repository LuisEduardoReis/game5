import { Entity } from "./entity"
import { Player } from "./player"
import { Enemy } from "./enemy"
import { stepTo, pointDistance3d, pushMatrices, popMatrices, setFill, setNoFill } from "../util"
import { doLevelVisibilityCheck } from "../collisions"
import { g3d } from ".."
import { playSound, playSoundAtPos, SOUNDS } from "../audio"
import { ExplosionCloud, ExplosionParticle } from "./particles"

export class Grenade extends Entity {
    constructor() {
        super()

        this.radius = 0.1
        this.hasGravity = true
        this.collidesWithLevel = true
        this.rolling = true

        this.timer = 3
    }

    update(delta) {
        super.update(delta)

        this.timer = stepTo(this.timer, 0, delta)
        if (this.timer == 0) {
            playSoundAtPos(SOUNDS.explosion, this.x, this.y, this.z, 0.1,1, 1,8)
            this.remove = true

            for(let i = 0; i < 10; i++) {
                let c = this.level.addEntity(new ExplosionCloud()).setPos(this.x, this.y, this.z)
                c.vx = 5 * (Math.random() - 0.5)
                c.vy = 5 * (Math.random() - 0.5)
                c.vz = 5 * (Math.random() - 0.5)
            }
            for(let i = 0; i < 100; i++) {
                let c = this.level.addEntity(new ExplosionParticle()).setPos(this.x, this.y - 0.5, this.z)
                c.vx = 5 * (Math.random() - 0.5)
                c.vy = -5 * (Math.random())
                c.vz = 5 * (Math.random() - 0.5)
            }

            this.level.entities.forEach(e => {
                if (!e.affectedByExternalForce) return
                let d = pointDistance3d(this.x, this.y, this.z, e.x, e.y, e.z)
                if (d > 4) return

                let hitscan = doLevelVisibilityCheck(this.level, this.x, this.y, this.z, e.x, e.y, e.z)
                if (!hitscan.visible) return                

                let force = 10
                e.ex += force * (e.x - this.x) / (d * d)
                e.ey += force * (e.y - this.y) / (d * d)
                e.ez += force * (e.z - this.z) / (d * d)

                let grenadeDamage = 250
                if (e instanceof Player || e instanceof Enemy) {
                    playSound(SOUNDS.hitmarker, 0.25)
                    e.damage(grenadeDamage / d)
                }
            });
        }
    }

    draw() {
        pushMatrices(g3d)
        g3d.translate(this.x, this.y, this.z)
        setFill([0.25,0.25,0.25,1.0])
        g3d.box(0.1)
        popMatrices(g3d)
        setNoFill()
    }
}