import { Entity } from "./entity";
import { WeaponMachineGun } from "../weapon";
import { Grenade } from "./grenade";
import { hasPointer, keysPressed, keysDown, p } from "..";
import { stepTo, clamp, lookAnglesToVector } from "../util";
import { TEXTURES } from "../graphics";
import { GRAVITY } from "./entity";
import { playSound, SOUNDS } from "../audio";

export const CRITICAL_HEALTH = 25
export const MOUSE_SENSIBILITY = 320

export class Player extends Entity {
    constructor() {
        super()

        this.radius = 0.2

        this.lookDirectionHorizontal = 0;
        this.lookDirectionVertical = 0;

        this.recoil = 0
        this.recoilFriction = 0.75

        this.fireCooldown = 0
        this.fireDelay = 0.1
        this.aimDownSights = 0

        this.screenShake = 0
        this.hurtAnimation = 0

        this.hasGravity = true
        this.collidesWithLevel = true
        this.collidesWithOthers = true
        this.bumpsWithOthers = true
        this.affectedByExternalForce = true
        this.damageableByExplosion = true

        this.weapon = new WeaponMachineGun()
        this.grenades = 2
    }

    update(delta) {
        super.update(delta)

        if(this.health > 0) {
            this.processInput(delta)
            if (this.weapon) this.weapon.update(delta)
        }
        
        if (this.recoil > 0.01) {
            this.lookDirectionVertical += this.recoil * delta
            this.lookDirectionVertical +=   0.25 * (1 - 2 * Math.random()) * delta
            this.lookDirectionHorizontal += 0.15 * (1 - 2 * Math.random()) * delta
        }
        this.recoil *= this.recoilFriction

        this.hurtAnimation = stepTo(this.hurtAnimation, 0, delta * 0.5)
        this.screenShake *= 0.95
    }

    processInput(delta) {
        if (hasPointer) {
            this.lookDirectionHorizontal -= p.movedX / MOUSE_SENSIBILITY
            this.lookDirectionVertical -= p.movedY / MOUSE_SENSIBILITY
            this.lookDirectionVertical = clamp(this.lookDirectionVertical, - 0.45 * Math.PI, 0.45 * Math.PI)
        }

        // Space
        if (keysPressed[32]) {
            this.vy = - 0.75 * GRAVITY
            this.y -= 0.01
        }        

        let speed = 3
        if (p.keyIsDown(87)) { // W
            this.x += speed * delta * Math.cos(this.lookDirectionHorizontal)
            this.z -= speed * delta * Math.sin(this.lookDirectionHorizontal)
        }
        if (p.keyIsDown(83)) { // S
            this.x -= speed * delta * Math.cos(this.lookDirectionHorizontal)
            this.z += speed * delta * Math.sin(this.lookDirectionHorizontal)
        }
        if (p.keyIsDown(65)) { // A
            this.x -= speed * delta * Math.sin(this.lookDirectionHorizontal)
            this.z -= speed * delta * Math.cos(this.lookDirectionHorizontal)            
        }
        if (p.keyIsDown(68)) { // D
            this.x += speed * delta * Math.sin(this.lookDirectionHorizontal)
            this.z += speed * delta * Math.cos(this.lookDirectionHorizontal)
        }

        // Mouse Left
        if (this.weapon) this.weapon.fire(this, 1)

        // Reload
        if (keysPressed["R".charCodeAt(0)]) this.weapon.reload() // R

        // Grenades
        if (keysPressed["E".charCodeAt(0)] && this.grenades > 0) {
            this.grenades--
            let g = this.level.addEntity(new Grenade()).setPos(this.x, this.y - this.radius, this.z)
            let d = lookAnglesToVector(this.lookDirectionHorizontal, this.lookDirectionVertical)
            g.vx = 10*d.x
            g.vy = 10*d.y
            g.vz = 10*d.z
        }

        // Mouse Right
        this.aimDownSights = stepTo(this.aimDownSights, keysDown[3] ? 1 : 0, 5 * delta) 


    }

    damage(v) {
        super.damage(v)
        this.hurtAnimation = 1
        this.screenShake = 30
        playSound(SOUNDS.player_hurt, 0.5)
    }

    collision(o) {
    }

    draw() {
        if (this.weapon) this.weapon.draw(this)
    }

    drawHUD() {
        if (this.weapon) this.weapon.drawHUD(this, p.windowWidth - 5, p.windowHeight - 5)
        for(let i = 0; i < this.grenades; i++) p.image(TEXTURES.grenade, p.windowWidth - 5 - 32 * (i+1), p.windowHeight - 80, 32,32)

        let lw = 30, lh = 200, lx = 5, ly = p.windowHeight - 5
        p.fill(96,0,0)
        p.rect(lx, ly - lh, lw, lh)
        p.fill(255,0,0); p.stroke(0); p.strokeWeight(1)
        let visibleHealth;
        if (this.health <= 0) visibleHealth = 0
        else if (this.health <= CRITICAL_HEALTH) visibleHealth = 0.1
        else visibleHealth = (this.health - CRITICAL_HEALTH + 10) / (100 - CRITICAL_HEALTH + 10)
        let ls = lh * visibleHealth

        if (this.health > CRITICAL_HEALTH || this.level.t % 0.5 < 0.25)
            p.rect(lx, ly - ls, lw, ls)
    }
}