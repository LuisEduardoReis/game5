import { player} from '..'
import { Entity } from "./entity"
import { ZombieGunner, Enemy } from "./enemy"
import { stepTo, pointDistance3d } from "../util"
import { doLevelVisibilityCheck } from '../collisions'

export class Spawner extends Entity {

    constructor() {
        super()

        this.spawnDelay = 2
        this.spawnTimer = Math.random() * this.spawnDelay
        
    }

    update(delta) {
        this.spawnTimer = stepTo(this.spawnTimer, 0, delta)
        if (this.spawnTimer == 0) {
            
            let numEnemies = this.level.entities.filter(e => e instanceof Enemy).length
            let distToPlayer = pointDistance3d(this.x, this.y, this.z, player.x, player.y, player.z)
            let visibilityCheck = !doLevelVisibilityCheck(this.level, this.x, this.y, this.z, player.x, player.y - player.radius, player.z).visible

            if (numEnemies < 10 && distToPlayer > 5 && distToPlayer < 16 && visibilityCheck) {
                this.level.addEntity(new ZombieGunner()).setPos(this.x + Math.random()*0.1, this.y, this.z + Math.random()*0.1)
            }
            this.spawnTimer = this.spawnDelay

        }
    }
}