import { Entity } from "./entity"
import { Player } from "./player"
import { WeaponMachineGun } from "../weapon"
import { TEXTURES } from "../graphics"
import { pushMatrices, popMatrices, pointDistanceSquared3d, stepTo } from "../util"
import { g3d, player } from ".."
import { theShader } from "../graphics"
import { playSound } from "../audio"
import { SOUNDS } from "../audio"


export const PICKUP_REGEN_MIN_DISTANCE = 6

export const PICKUP_TYPES = {
    MACHINE_GUN: 0,
    GRENADE: 1,
    HEALTH_PACK: 2
}
 

export class Pickup extends Entity {

    constructor() {
        super()

        this.sprite = TEXTURES.test
        this.available = true
        this.collidesWithOthers = true

        this.animTimer = Math.random()
        this.regenTimer = 0
        this.regenDelay = 30

        this.generate()
    }

    generate() {
        this.type = Math.floor(Math.random() * Object.keys(PICKUP_TYPES).length)
        let pickupSprites = {}
        pickupSprites[PICKUP_TYPES.MACHINE_GUN] = TEXTURES.machine_gun
        pickupSprites[PICKUP_TYPES.GRENADE] = TEXTURES.grenade
        pickupSprites[PICKUP_TYPES.HEALTH_PACK] = TEXTURES.health_pack
        this.sprite = pickupSprites[this.type]
    }

    update(delta) {
        this.animTimer += delta

        if (!this.available) {
            let distToPlayer = pointDistanceSquared3d(this.x, this.y, this.z, player.x, player.y, player.z)
            if (distToPlayer > PICKUP_REGEN_MIN_DISTANCE * PICKUP_REGEN_MIN_DISTANCE) {
                this.regenTimer = stepTo(this.regenTimer, 0, delta)
            }
            if (this.regenTimer == 0) {
                this.generate()
                this.available = true                
            }
        }
    }

    collision(o) {
        if (o instanceof Player && this.available) {
            this.available = this.collect(o)
            if (!this.available) this.regenTimer = this.regenDelay
        }
    }

    collect(player) {
        console.log(this.type, player.health)
        if (this.type == PICKUP_TYPES.MACHINE_GUN) {
            if (player.weapon == null) {
                player.weapon = new WeaponMachineGun()
                playSound(SOUNDS.machine_gun_reload)
                return false
            } else if (player.weapon instanceof WeaponMachineGun && player.weapon.ammo < player.weapon.maxAmmo) {
                player.weapon.ammo = Math.min(player.weapon.ammo + 180, player.weapon.maxAmmo)
                playSound(SOUNDS.pickup)
                return false
            } else return true
        } else if (this.type == PICKUP_TYPES.GRENADE) {
            if (player.grenades < 4) {
                player.grenades = 4
                playSound(SOUNDS.pickup)
                return false
            } else return true
        } else if (this.type == PICKUP_TYPES.HEALTH_PACK) {
            if (player.health < 100) {
                player.health = 100
                playSound(SOUNDS.health_regen)
                return false
            } else return true
        }
        return true
    }

    draw() {
        if (this.available) {
            pushMatrices(g3d)
            theShader.setUniform("texture", this.sprite)
            theShader.setUniform("flipTexU", false)
            g3d.translate(this.x, this.y, this.z)
            g3d.rotateY(Math.atan2(player.x - this.x, player.z - this.z) + (this.animTimer % Math.PI) - Math.PI/2)
            g3d.plane(0.25, 0.25)
            g3d.rotateY(Math.PI)
            theShader.setUniform("flipTexU", true)
            g3d.plane(0.25, 0.25)
            popMatrices(g3d)
        }
    }
}