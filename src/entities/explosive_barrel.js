export class ExplosiveBarrel extends Entity {
    constructor() {
        super() 

        this.hitScanTarget = true
        this.sprite = TEXTURES.explosive_barrel

        this.spriteW = 0.5
        this.spriteH = 0.5
        this.hitBoxWidth = 6 / 16
        this.hitBoxBottom = 0.25
        this.hitBoxTop = 0.25 - 0.5/16
    }
}