import { g3d, p } from ".."
import { setNoFill, setFill, pushMatrices, popMatrices, randomRange } from "../util"
import { Entity } from "./entity"
import { TEXTURES } from "../graphics"

export class Particle extends Entity {
    constructor() {
        super()

        this.lifetime = 1
        this.friction = 1
        this.rolling = true
    }

    update(delta) {
        super.update(delta)

        this.lifetime -= delta
        
        this.vx *= this.friction
        this.vy *= this.friction
        this.vz *= this.friction

        if (this.lifetime < 0) {
            this.remove = true
        }
    }
}

export class BulletHole extends Particle {
    
    constructor() {
        super()

        this.lifetime = 10
    }

    draw() {
        pushMatrices(g3d)
        setFill([0,0,0,1])
        g3d.translate(this.x, this.y, this.z)    
        g3d.box(0.025)
        setNoFill()
        popMatrices(g3d)
    }

}

export class BulletFrag extends Particle {
    
    constructor(hitscan) {
        super()

        this.color = p.color(hitscan.hitColor)._array
        this.radius = 0.015
        this.lifetime = 1
        this.hasGravity = true
        this.collidesWithLevel = true

        this.vx = hitscan.nx + 2 * (Math.random() - 0.5)
        this.vy = hitscan.ny + 2 * (Math.random() - 0.5)
        this.vz = hitscan.nz + 2 * (Math.random() - 0.5)
    }

    draw() {
        pushMatrices(g3d)
        setFill(this.color)
        g3d.translate(this.x, this.y, this.z)    
        g3d.box(0.015)
        setNoFill()
        popMatrices(g3d)
    }
}

export class ExplosionCloud extends Particle {

    constructor() {
        super()

        this.lifetime = randomRange(0.5,1.5)
        this.spriteW = 2
        this.spriteH = 2

        this.sprite = TEXTURES.explosion_cloud
        this.friction = 0.95
    }
}

export class ExplosionParticle extends Particle {
    constructor() {
        super()

        this.lifetime = randomRange(2,3)
        this.radius = 0.1
        this.hasGravity = true
        this.collidesWithLevel = true
        this.graycolor = (Math.random() * 96) / 255
    }

    draw() {
        pushMatrices(g3d)
        g3d.fill(this.graycolor)
        setFill([this.graycolor, this.graycolor, this.graycolor,1.0])
        g3d.translate(this.x, this.y + this.radius/2, this.z)    
        g3d.box(0.025)
        setNoFill()
        popMatrices(g3d)
    }
}