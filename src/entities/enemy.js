import { player } from ".."
import { Entity } from "./entity"
import { TEXTURES } from "../graphics"
import { stepTo, pointDistance3d, pointDistance2d, between, newPoint } from "../util"
import { doHitScanLevel } from "../collisions"
import { findPath } from "../pathfinding"
import { playSoundAtPos, SOUNDS } from "../audio"
import { p } from '..'

export class Enemy extends Entity {
    constructor() {
        super()
        this.hasGravity = true
        this.hitScanTarget = true
        this.collidesWithLevel = true
        this.collidesWithOthers = true
        this.bumpsWithOthers = true
        this.affectedByExternalForce = true
        this.damageableByExplosion = true

        this.color = "#7F0000"
    }

    die() {
        super.die()
        this.rolling = true
        this.deadAnimationTimer = this.deadAnimationDelay
        this.hitScanTarget = false
    }
}

export const PATHFIND_SEARCH_COOLOFF = 1
export const ENEMY_SHOOT_RANGE = 5
export const ENEMY_MIN_DIST_TO_PLAYER = 2


export class ZombieGunner extends Enemy {
    constructor() {
        super()

        this.radius = 0.3
        this.sprite = TEXTURES.zombie_gunner_shooting
        this.spriteW = 2 * this.radius
        this.spriteH = 2 * this.radius

        this.animDelay = 0.35
        this.animTimer = Math.random() * this.animDelay
        this.animIndex = 0
        
        this.deadAnimationTimer = 0
        this.deadAnimationDelay = 5

        this.hitBoxWidth = this.spriteW * 0.6
        this.hitBoxTop = this.radius
        this.hitBoxBottom = this.radius

        this.targetNode = null
        this.pathFindCoolOffTimer = 0
        this.pathFindRefreshTimer = 0

        this.fireCooldown = 0
        this.fireDelay = 0.5
        this.fireWarmup = 0
        this.fireWarmupDelay = 1
    }

    update(delta) {
        super.update(delta)

        let canShootPlayer = false
        let shooting = false
        let walking = false

        if (!this.dead) {
            // Shooting        
            let distToPlayer3d = pointDistance3d(player.x, player.y, player.z, this.x, this.y, this.z)    
            if (distToPlayer3d < ENEMY_SHOOT_RANGE) {
                let lineOfSight3d = doHitScanLevel(this.level, this.x, this.y, this.z,
                    (player.x - this.x) / distToPlayer3d, 
                    (player.y - player.radius - this.y) / distToPlayer3d, 
                    (player.z - this.z) / distToPlayer3d
                )
                if (lineOfSight3d.distance > distToPlayer3d) canShootPlayer = true
            }
            this.fireCooldown = stepTo(this.fireCooldown, 0, delta)
            this.fireWarmup = canShootPlayer ? stepTo(this.fireWarmup, 0, delta) : this.fireWarmupDelay

            if (canShootPlayer && this.fireWarmup == 0 && player.health > 0) {
                shooting = true
                if (this.fireCooldown == 0 && (this.animIndex == 0 || this.animIndex == 2)) {
                    this.fireCooldown = this.fireDelay
                    playSoundAtPos(SOUNDS.machine_gun_shoot_enemy, this.x,this.y,this.z, 0, 0.3, ENEMY_MIN_DIST_TO_PLAYER, ENEMY_SHOOT_RANGE)
                    let oddsOfHitting = p.map(distToPlayer3d, ENEMY_MIN_DIST_TO_PLAYER, ENEMY_SHOOT_RANGE, 0.85,0.1, true)
                    if (Math.random() < oddsOfHitting) {
                        player.damage(5)
                    }
                }
            }
            
            let needsNewNode = (cn) => !cn || !between(pointDistance2d(cn.x, cn.z, this.x, this.z), 0.5,2) || this.pathFindRefreshTimer == 0
            let distToPlayer2d = pointDistance2d(player.x, player.z, this.x, this.z)
            let lineOfSight2d = doHitScanLevel(this.level, this.x, this.y - this.radius, this.z,
                (player.x - this.x) / distToPlayer2d, 0, (player.z - this.z) / distToPlayer2d)

            // Line of sight pathfinding        
            if (Math.abs(this.y - player.y) < 1 && lineOfSight2d.distance > distToPlayer2d) {
                this.testPath = null
                this.targetNode = newPoint(player.x, player.y, player.z)
            }

            // Regular pathfinding
            else if (this.pathFindCoolOffTimer == 0 && distToPlayer3d > ENEMY_MIN_DIST_TO_PLAYER && needsNewNode(this.targetNode)) {
                let path = findPath(this.level, this.x,this.y,this.z, player.x, player.y, player.z)
                this.testPath = path
                this.targetNode = null

                while(path.length > 0 && needsNewNode(this.targetNode)) {
                    this.targetNode = path.shift()
                    this.targetNode.x += 0.5; this.targetNode.y += 0.5; this.targetNode.z += 0.5
                }

                if (!this.targetNode) this.pathFindCoolOffTimer = PATHFIND_SEARCH_COOLOFF
                else this.pathFindRefreshTimer = PATHFIND_SEARCH_COOLOFF
            }

            // Movement
            if (this.targetNode && distToPlayer3d > ENEMY_MIN_DIST_TO_PLAYER) {
                walking = true
                let distToNode = pointDistance2d(this.targetNode.x, this.targetNode.z, this.x, this.z)
                if (distToNode > 0.1) {
                    this.x += delta * (this.targetNode.x - this.x) / distToNode
                    this.z += delta * (this.targetNode.z - this.z) / distToNode
                }
            }

            this.pathFindCoolOffTimer = stepTo(this.pathFindCoolOffTimer, 0, delta)
            this.pathFindRefreshTimer = stepTo(this.pathFindRefreshTimer, 0, delta)    
        }    

        // Death animation
        if (this.dead) {
            this.deadAnimationTimer = stepTo(this.deadAnimationTimer, 0, delta)
            if (this.deadAnimationTimer == 0) {
                this.remove = true
            }
        }

        // Sprite
        if (this.dead) {
            this.sprite = TEXTURES.zombie_gunner_dead
        } else if (walking) {
            this.sprite = shooting ? TEXTURES.zombie_gunner_walking_shooting : TEXTURES.zombie_gunner_walking
        } else {
            this.sprite = shooting ? TEXTURES.zombie_gunner_shooting : TEXTURES.zombie_gunner
        }
    }

    draw() {
        super.draw()

        /*
        if (this.testPath) {
            g3d.fill(255)
            this.testPath.forEach(n => {
                pushMatrices(g3d)
                g3d.translate(n.x+0.5,n.y+0.5,n.z+0.5)
                g3d.sphere(0.1)
                popMatrices(g3d)
            });
        }
        if (this.targetNode) {
            g3d.fill(255,0,0)
            pushMatrices(g3d)
            g3d.translate(this.targetNode.x,this.targetNode.y,this.targetNode.z)
            g3d.sphere(0.15)
            popMatrices(g3d)
        }*/
    }    
}