import { clampAbsoluteValue, stepTo, setUniformTexture, pushMatrices, popMatrices } from "../util"
import { g3d, player } from ".."
import { theShader } from "../graphics"

export const GRAVITY = 5
export const GROUND_FRICTION = 0.90
export const EXTERNAL_FORCE_FRICTION = 0.9

let ENTITY_ID_COUNTER = 0

export class Entity {
    constructor() {
        this.id = ++ENTITY_ID_COUNTER

        this.x = 0; this.y = 0; this.z = 0
        this.vx = 0; this.vy = 0; this.vz = 0
        this.px = 0; this.py = 0; this.pz = 0
        this.ex = 0; this.ey = 0; this.ez = 0
        this.level = null
        this.radius = 0.2
        this.color = "#ffffff"
        
        this.health = 100
        this.dead = false
        this.remove = false

        this.hasGravity = false
        this.hitScanTarget = false
        this.collidesWithLevel = false
        this.collidesWithOthers = false
        this.bumpsWithOthers = false
        this.rolling = false
        this.affectedByExternalForce = false
        this.damageableByExplosion = false
        

        this.sprite = null
        this.spriteW = 1
        this.spriteH = 1

        this.animIndex = 0
        this.animTimer = -1
        this.animDelay = 0.1

        this.hitBoxTop = 0.5
        this.hitBoxBottom = 0.5
        this.hitBoxWidth = 1
    }

    preupdate() {
        this.px = this.x; this.py = this.y; this.pz = this.z
    }

    update(delta) {
        if (this.hasGravity) this.vy += GRAVITY * delta

        if (this.collidesWithLevel) {
            let maxSpeed = (this.radius - 0.0001) / delta
            this.vx = clampAbsoluteValue(this.vx, 0, maxSpeed)
            this.vy = clampAbsoluteValue(this.vy, 0, maxSpeed)
            this.vz = clampAbsoluteValue(this.vz, 0, maxSpeed)
        }

        this.x += (this.vx + this.ex) * delta
        this.y += (this.vy + this.ey) * delta
        this.z += (this.vz + this.ez) * delta

        if (this.affectedByExternalForce)
            this.ex *= EXTERNAL_FORCE_FRICTION; this.ey *= EXTERNAL_FORCE_FRICTION; this.ez *= EXTERNAL_FORCE_FRICTION;

        if (this.health <= 0 && !this.dead) {
            this.die()
            this.dead = true
        }

        if (this.animTimer != -1 && Array.isArray(this.sprite)) {
            this.animTimer = stepTo(this.animTimer, 0, delta)
            if (this.animTimer == 0) {
                this.animIndex = (this.animIndex + 1) % this.sprite.length
                this.animTimer = this.animDelay
            }
        }
    }

    damage(v) {
        this.health = stepTo(this.health, 0, v)
    }

    die() {}

    setPos(x,y,z) {
        this.x = x
        this.y = y
        this.z = z
        return this
    }

    collision(o) {

    }

    draw() {
        if (this.sprite) {
            if (Array.isArray(this.sprite)) setUniformTexture(theShader, this.sprite[this.animIndex % this.sprite.length])
            else setUniformTexture(theShader, this.sprite)

            pushMatrices(g3d)
            g3d.translate(this.x, this.y, this.z)
            g3d.rotateY(Math.atan2(player.x - this.x, player.z - this.z))
            g3d.plane(this.spriteW,this.spriteH)
            popMatrices(g3d)
        }
    }
}