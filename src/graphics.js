import { p, g3d } from "."

export const TEXTURES = {}
export const TILE_SIZE = 16
export const CHAR_SIZE = 32
export let theShader

export function loadShaders() {
    theShader = p.loadShader("shaders/shader.vert", "shaders/shader.frag")
}

export function loadTextures() {
    TEXTURES.test = p.loadImage("assets/test.png")
    TEXTURES.map1 = p.loadImage("assets/map1.png")
    TEXTURES.tilemap = p.loadImage("assets/tileset.png")
}

export function setupGraphics() {
    let gl = g3d._renderer.GL
    gl.enable(gl.CULL_FACE)
    gl.cullFace(gl.FRONT)

    gl.depthMask(true);
    gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
    gl.enable(gl.BLEND);

    createTileTextures()

    Object.keys(TEXTURES).forEach(t => {
        if (Array.isArray(TEXTURES[t])) TEXTURES[t].forEach(setTextureInterpolation)
        else setTextureInterpolation(TEXTURES[t])        
    })
}

function setTextureInterpolation(t) {
    g3d._renderer.getTexture(t).setInterpolation(p.NEAREST, p.NEAREST)
}

export function createTileTextures() {
    TEXTURES.metal_plate = getTile(2,0,TILE_SIZE)
    TEXTURES.metal_rust = getTile(3,0,TILE_SIZE)
    TEXTURES.brick = getTile(4,0,TILE_SIZE)
    TEXTURES.dirt = getTile(5,0,TILE_SIZE)

    TEXTURES.machine_gun = getTile(0,1,TILE_SIZE)
    TEXTURES.machine_gun.loadPixels()
    TEXTURES.grenade = getTile(1,1,TILE_SIZE)
    TEXTURES.health_pack = getTile(2,1,TILE_SIZE)

    TEXTURES.explosion_cloud = getTile(3,1,TILE_SIZE)

    TEXTURES.zombie_gunner = getTile(0,1, CHAR_SIZE)
    TEXTURES.zombie_gunner_dead = getTile(1,2, CHAR_SIZE)
    TEXTURES.zombie_gunner_step1 = getTile(0,2, CHAR_SIZE)
    TEXTURES.zombie_gunner_step2 = getTile(0,3, CHAR_SIZE)
    TEXTURES.zombie_gunner_shoot = getTile(1,1, CHAR_SIZE)
    TEXTURES.zombie_gunner_walking = [
        TEXTURES.zombie_gunner,
        TEXTURES.zombie_gunner_step1,
        TEXTURES.zombie_gunner,
        TEXTURES.zombie_gunner_step2
    ]
    TEXTURES.zombie_gunner_walking_shooting = [
        TEXTURES.zombie_gunner_shoot,
        TEXTURES.zombie_gunner_step1,
        TEXTURES.zombie_gunner_shoot,
        TEXTURES.zombie_gunner_step2
    ]
    TEXTURES.zombie_gunner_shooting = [
        TEXTURES.zombie_gunner_shoot,
        TEXTURES.zombie_gunner,        
    ]
}

function getTile(x,y,s) {
    return TEXTURES.tilemap.get(x * s, y * s, s, s);
}

export function getPixelValue(pixels, i) {
    let v = 0
    v += pixels[i] << 16
    v += pixels[i+1] << 8
    v += pixels[i+2]
    return v
}