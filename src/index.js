import * as p5 from 'p5';
import { loadShaders, loadTextures, setupGraphics } from './graphics';
import { loadAudio, updateSounds } from './audio';
import { createModels } from './models'
import { setupTiles } from './tiles';
import { Level } from './level';
import { Player } from './entities/player';
import { theShader } from './graphics';
import { CRITICAL_HEALTH } from './entities/player';

export const FPS = 60
export let g3d
export let canvas
export let keysPressed = {}
export let keysDown = {}
export let hasPointer = false

export let level
export let player

export const sketch = (p) => {
    let frameTimePercentageRollingAverage = 50    

    p.preload = () => {
        loadShaders()
        loadTextures()
        loadAudio()
    }

    p.setup = () => {
        canvas = p.createCanvas(p.windowWidth, p.windowHeight)
        g3d = p.createGraphics(p.windowWidth, p.windowHeight, p.WEBGL)
        canvas.style('display', 'block')
        document.addEventListener('pointerlockchange', (e) =>{ hasPointer = !hasPointer }, false)
        p.frameRate(FPS)

        setupGraphics()
        createModels()
        setupTiles()

        level = new Level(16)
        player = level.addEntity(new Player())
        if (level.spawn) {
            player.x = level.spawn.x
            player.y = level.spawn.y
            player.z = level.spawn.z
        }
    }

    p.draw = () => {
        let beforeDrawTimestamp = window.performance.now()
        g3d.clear(0,0,0,1)
        g3d.background(32)

        
        level.update(1 / FPS)

        updateSounds(1 / FPS)
        Object.keys(keysPressed).forEach(key => keysPressed[key] = false)

        g3d.perspective(p.radians(60), p.width/p.height, 0.01,1000);
        g3d.camera(
            player.x, player.y - player.radius + 0.01, player.z, 
            player.x + Math.cos(player.lookDirectionHorizontal), 
            player.y - player.radius - Math.tan(player.lookDirectionVertical), 
            player.z - Math.sin(player.lookDirectionHorizontal), 
            0,1,0
        )
        
        g3d.shader(theShader)
        level.draw()
        

        let ox = 0, oy = 0;
        if (player.screenShake > 0) {
            ox += player.screenShake * (Math.random() - 0.5)
            oy += player.screenShake * (Math.random() - 0.5)
        }
        
        p.image(g3d,ox,oy)

        if(player.hurtAnimation > 0 || player.health < CRITICAL_HEALTH) {
            p.fill(128,0,0, 128 * (player.health < CRITICAL_HEALTH ? 1 : player.hurtAnimation))
            p.rect(0,0,p.width,p.height)
        }

        player.drawHUD()

        p.fill(255); p.textAlign(p.LEFT, p.BOTTOM)
        p.text(Math.round(p.frameRate()) + " fps", 5,30)
        
        let frameTime = window.performance.now() - beforeDrawTimestamp
        let frameTimePercentage = frameTime * FPS / 10 //frameTime / (1 / FPS) / 1000 * 100
        let rollingAverage = 0.99
        frameTimePercentageRollingAverage = frameTimePercentageRollingAverage * rollingAverage + frameTimePercentage * (1 - rollingAverage)
        p.text(Math.round(frameTimePercentageRollingAverage) + "% cpu", 5, 60)
    }

    p.mousePressed = (event) => {
        if (!hasPointer) {
            canvas.elt.requestPointerLock()
        } else {
            keysPressed[event.which] = true
            keysDown[event.which] = true
        }
    }
    
    p.mouseReleased = (event) => {
        keysDown[event.which] = false
    }
    
    p.keyPressed = () => {
        keysPressed[p.keyCode] = true
        keysDown[p.keyCode] = true
    
        if (p.keyCode == 'M'.charCodeAt(0)) muted = !muted
        if (p.keyCode == 'T'.charCodeAt(0)) level.generateModel()
    }
    p.keyReleased = () => {
        keysDown[p.keyCode] = false
    }
    
    p.windowResized = () => {
        p.resizeCanvas(p.windowWidth, p.windowHeight)
        g3d.resizeCanvas(p.windowWidth, p.windowHeight)
    }
}

export const p = new p5(sketch, document.body);