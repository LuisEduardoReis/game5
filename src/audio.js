import { Howl }from 'howler'
import { player, p } from '.'
import { stepTo, pointDistance3d } from './util'


export const SOUNDS = {}
const DEFAULT_SOUND_COOLDOWN = 0.05
let muted = false

export class SoundEffect {
    constructor(file, nInstances) {
        nInstances ||= 1
        this.instances = []
        this.instanceIndex = 0

        for(let i = 0; i < nInstances; i++) this.instances.push(new Howl({
            src: [file]
        }))
    }

    play(vol) {
        this.instances[this.instanceIndex].volume(vol)
        this.instances[this.instanceIndex++].play()
        this.instanceIndex %= this.instances.length
    }
}


export function loadAudio() {
    SOUNDS.machine_gun_shoot = new SoundEffect("assets/machine_gun_shoot2.wav")
    SOUNDS.machine_gun_shoot_enemy = new SoundEffect("assets/machine_gun_shoot2.wav")
    SOUNDS.machine_gun_reload = new SoundEffect("assets/machine_gun_reload.mp3")
    SOUNDS.machine_gun_click = new SoundEffect("assets/machine_gun_click.wav")
    SOUNDS.explosion = new SoundEffect("assets/explosion.wav")
    SOUNDS.player_hurt = new SoundEffect("assets/player_hurt.wav")
    SOUNDS.hitmarker = new SoundEffect("assets/hit_marker.wav")
    SOUNDS.health_regen = new SoundEffect("assets/health_regen.wav")
    SOUNDS.pickup = new SoundEffect("assets/pickup.wav")
    
    Object.keys(SOUNDS).forEach(k => {
        SOUNDS[k].cooldownDelay = DEFAULT_SOUND_COOLDOWN
    })
    SOUNDS.hitmarker.cooldownDelay = 0.1
    SOUNDS.hitmarker.cooldownPerEntity = {}
}


export function updateSounds(delta) {
    Object.keys(SOUNDS).forEach(k => {
        SOUNDS[k].cooldown = stepTo(SOUNDS[k].cooldown || 0, 0, delta)
    })
}

export function playSound(sound, vol) {
    if (!vol) vol = 1
    if (!muted && sound.cooldown == 0) {
        sound.play(vol)
        sound.cooldown = sound.cooldownDelay
    }
}

export function playSoundAtPos(sound,x,y,z,minVol,maxVol,minDist,maxDist) {
    if (!muted && sound.cooldown == 0) {
        let dist = pointDistance3d(x,y,z, player.x,player.y,player.z)
        let vol = p.map(dist, minDist,maxDist, maxVol, minVol, true)
        if (vol > 0) {
            sound.play(vol)
        }
        sound.cooldown = sound.cooldownDelay
    }
}