import { pointDistanceSquared3d, pointDistance3d, lineToPlane, between, pointDistance2d } from "./util";
import { GROUND_FRICTION } from "./entities/entity";


export function doLevelCollisions(level, delta) {
    level.entities.forEach(e => {
        if (!e.collidesWithLevel) return;
        if (isNaN(e.x) || isNaN(e.y) || isNaN(e.z)) console.log(e)
        
        let xc = Math.floor(e.x)
        let yc = Math.floor(e.y)
        let zc = Math.floor(e.z)

        let xr = e.x - xc
        let yr = e.y - yc
        let zr = e.z - zc

        if (level.getTile(xc,yc,zc).type.solid_block) {
            e.x = e.px
            e.z = e.pz
            xc = Math.floor(e.x)              
            zc = Math.floor(e.z)
            xr = e.x - xc
            zr = e.z - zc
        }

        // Y collisions
        if (level.getTile(xc, yc - 1, zc).type.solid_block && yr <= e.radius) {
            yr = e.radius
            e.vy = 0
            e.vx *= GROUND_FRICTION; e.vz *= GROUND_FRICTION
        }
        if (level.getTile(xc, yc + 1, zc).type.solid_block && yr >= 1 - e.radius) {
            yr = 1 - e.radius
            e.vy = 0
            e.vx *= GROUND_FRICTION; e.vz *= GROUND_FRICTION
        }
        e.y = yc + yr
        yc = Math.floor(e.y)

        let feet_yc = Math.floor(e.y + e.radius - 0.001)
        let feet_tile = level.getTile(xc, feet_yc, zc)
        if (feet_tile.type.ramp && e.vy >= 0) {
            // SOUTH
            if (feet_tile.ramp_direction == '-x' && e.y > feet_yc + (1 - xr - e.radius)) {
                yc = feet_yc
                yr = 1 - xr - e.radius
                e.vy = 0
                e.vz *= GROUND_FRICTION
                if (e.rolling) e.vx -= 1
            }
            // NORTH
            if (feet_tile.ramp_direction == '+x' && e.y > feet_yc + (xr - e.radius)) {
                yc = feet_yc
                yr = xr - e.radius
                e.vy = 0
                e.vz *= GROUND_FRICTION
                if (e.rolling) e.vx += 1
            }
            // NORTH
             if (feet_tile.ramp_direction == '-z' && e.y > feet_yc + (1 - zr - e.radius)) {
                yc = feet_yc
                yr = 1 - zr - e.radius
                e.vy = 0
                e.vx *= GROUND_FRICTION
                if (e.rolling) e.vz -= 1
            }
             // SOUTH
            if (feet_tile.ramp_direction == '+z' && e.y > feet_yc + (zr - e.radius)) {
                yc = feet_yc
                yr = zr - e.radius
                e.vy = 0
                e.vx *= GROUND_FRICTION
                if (e.rolling) e.vz += 1
            }
        }
        e.y = yc + yr
        yc = Math.floor(e.y)

        // X collisions
        let west_tile = level.getTile(xc - 1, yc, zc)
        if ((west_tile.type.solid_block || (west_tile.type.ramp && west_tile.ramp_direction != '+x')) && xr <= e.radius) {
            xr = e.radius
            e.vx = 0
        }
        let east_tile = level.getTile(xc + 1, yc, zc)
        if ((east_tile.type.solid_block || (east_tile.type.ramp && east_tile.ramp_direction != '-x')) && xr >= 1 - e.radius) {
            xr = 1 - e.radius
            e.vx = 0
        }

        e.x = xc + xr
        xc = Math.floor(e.x)

        
        // Z collisions
        let north_tile = level.getTile(xc, yc, zc - 1)
        if ((north_tile.type.solid_block || (north_tile.type.ramp && north_tile.ramp_direction != '+z')) && zr <= e.radius) {
            zr = e.radius
            e.vz = 0
        }
        let south_tile = level.getTile(xc, yc, zc + 1)
        if ((south_tile.type.solid_block || (south_tile.type.ramp && south_tile.ramp_direction != '-z')) && zr >= 1 - e.radius) {
            zr = 1 - e.radius
            e.vz = 0
        }

        e.z = zc + zr

    })
}

export function doEntityCollisions(level) {
    level.entities.forEach(e => {
        if (!e.collidesWithOthers) return
        level.entities.forEach(o => {
            if (e == o || !o.collidesWithOthers) return

            let distSquared = pointDistanceSquared3d(e.x,e.y,e.z, o.x,o.y,o.z)
            let radiusSum = e.radius + o.radius

            if (distSquared > radiusSum*radiusSum) return

            e.collision(o)
            if (e.bumpsWithOthers && o.bumpsWithOthers) {
                let dist = Math.sqrt(distSquared)
                let overlap = dist - radiusSum
                if (dist > 0) {
                    o.x += overlap/2 * (e.x - o.x) / dist
                    o.z += overlap/2 * (e.z - o.z) / dist
                }
            }
        })
    })
}

export function doHitScanLevel(level, posX,posY,posZ, rayDirX,rayDirY,rayDirZ) {
    let result = {}
    result.hit = false
    result.hitDirection = null
    result.nx = 0; result.ny = 0; result.nz = 0;

    let mapX = Math.floor(posX)
    let mapY = Math.floor(posY)
    let mapZ = Math.floor(posZ)
    let deltaDistX = rayDirX == 0 ? 1e30 : Math.abs(1 / rayDirX)
    let deltaDistY = rayDirY == 0 ? 1e30 : Math.abs(1 / rayDirY)
    let deltaDistZ = rayDirZ == 0 ? 1e30 : Math.abs(1 / rayDirZ)

    let sideDistX, sideDistY, sideDistZ
    let stepX, stepY, stepZ

    if (rayDirX < 0) {
        stepX = -1;
        sideDistX = (posX - mapX) * deltaDistX;
    } else {
        stepX = 1;
        sideDistX = (mapX + 1.0 - posX) * deltaDistX;
    }
    if (rayDirY < 0) {
        stepY = -1;
        sideDistY = (posY - mapY) * deltaDistY;
    } else {
        stepY = 1;
        sideDistY = (mapY + 1.0 - posY) * deltaDistY;
    }
    if (rayDirZ < 0) {
        stepZ = -1;
        sideDistZ = (posZ - mapZ) * deltaDistZ;
    } else {
        stepZ = 1;
        sideDistZ = (mapZ + 1.0 - posZ) * deltaDistZ;
    }

    // Check if inside ramp already
    let checkRampSlope = (tile) => {
        let rampHit
        if (tile.ramp_direction == "-x") rampHit = lineToPlane(posX,posY,posZ,  posX + rayDirX, posY + rayDirY, posZ + rayDirZ,  mapX,mapY+1,mapZ,   -1,-1,0)
        if (tile.ramp_direction == "+x") rampHit = lineToPlane(posX,posY,posZ,  posX + rayDirX, posY + rayDirY, posZ + rayDirZ,  mapX+1,mapY+1,mapZ, 1,-1,0)
        if (tile.ramp_direction == "-z") rampHit = lineToPlane(posX,posY,posZ,  posX + rayDirX, posY + rayDirY, posZ + rayDirZ,  mapX,mapY+1,mapZ,   0,-1,-1)
        if (tile.ramp_direction == "+z") rampHit = lineToPlane(posX,posY,posZ,  posX + rayDirX, posY + rayDirY, posZ + rayDirZ,  mapX,mapY+1,mapZ+1, 0,-1,1)

        if (rampHit && between(rampHit.x,mapX,mapX+1) && between(rampHit.y,mapY,mapY+1) && between(rampHit.z,mapZ,mapZ+1)) {
            result.hit = true
            result.distance = rampHit.distance

            let sq = Math.SQRT2 / 2
            if (tile.ramp_direction == "-x") { result.nx = -sq; result.ny = -sq; result.nz =   0 }
            if (tile.ramp_direction == "+x") { result.nx =  sq; result.ny = -sq; result.nz =   0 }
            if (tile.ramp_direction == "-z") { result.nx =   0; result.ny = -sq; result.nz = -sq }
            if (tile.ramp_direction == "+z") { result.nx =   0; result.ny = -sq; result.nz =  sq }
        }
    }
    if (level.getTile(mapX, mapY, mapZ).type.ramp) checkRampSlope(level.getTile(mapX, mapY, mapZ))

    // Perform DDA
    result.iterations = 0;
    while (!result.hit && result.iterations++ < 100) {
        //jump to next map square, either in x-direction, y-direction or z-direction
        if (sideDistX < sideDistY && sideDistX < sideDistZ) {
            sideDistX += deltaDistX;
            mapX += stepX;
            result.hitDirection = 'x'
            result.distance = sideDistX - deltaDistX
        } else if (sideDistY < sideDistZ) {
            sideDistY += deltaDistY;
            mapY += stepY;
            result.hitDirection = 'y'
            result.distance = sideDistY - deltaDistY
        } else {
            sideDistZ += deltaDistZ;
            mapZ += stepZ;
            result.hitDirection = 'z'
            result.distance = sideDistZ - deltaDistZ
        }

        
        let tile = level.getTile(mapX, mapY, mapZ)
        //Check if ray has hit a wall
        if (tile.type.solid_block) {
            result.hit = true
            if (result.hitDirection == "x") { result.nx = -stepX; result.ny = 0; result.nz = 0}
            if (result.hitDirection == "y") { result.nx = 0; result.ny = -stepY; result.nz = 0}
            if (result.hitDirection == "z") { result.nx = 0; result.ny = 0; result.nz = -stepZ}

        //Check if ray has hit a ramp
        } else if (tile.type.ramp) {        
            let hitX = posX + rayDirX * result.distance
            let hitY = posY + rayDirY * result.distance
            let hitZ = posZ + rayDirZ * result.distance
            let hitXRel = hitX - Math.floor(hitX)
            let hitYRel = hitY - Math.floor(hitY)
            let hitZRel = hitZ - Math.floor(hitZ)

            // Ramp sides
                 if (tile.ramp_direction == "-x" && hitXRel + hitYRel > 1 && result.hitDirection == 'z') { result.hit = true; result.nz = -stepZ }
            else if (tile.ramp_direction == "+x" && hitXRel < hitYRel     && result.hitDirection == 'z') { result.hit = true; result.nz = -stepZ }
            else if (tile.ramp_direction == "-z" && hitZRel + hitYRel > 1 && result.hitDirection == 'x') { result.hit = true; result.nx = -stepX }
            else if (tile.ramp_direction == "+z" && hitZRel < hitYRel     && result.hitDirection == 'x') { result.hit = true; result.nx = -stepX }
            // Ramp slope
            else checkRampSlope(tile)
        }
    }

    if (result.hit) {
        result.hitMapX = mapX; result.hitMapY = mapY; result.hitMapZ = mapZ
        result.hitTile = level.getTile(mapX, mapY, mapZ)
        result.x = posX + rayDirX * result.distance
        result.y = posY + rayDirY * result.distance
        result.z = posZ + rayDirZ * result.distance
        result.levelHit = true
    }
    return result
}

export function doLevelVisibilityCheck(level, ax,ay,az, bx,by,bz) {
    let dist = pointDistance3d(ax,ay,az, bx,by,bz)
    let dx = (bx - ax) / dist
    let dy = (by - ay) / dist
    let dz = (bz - az) / dist;

    let hitscan = doHitScanLevel(level, ax,ay,az, dx,dy,dz)

    hitscan.visible = hitscan.distance > dist

    return hitscan
}


export function doHitScanEntities(level, posX,posY,posZ, rayDirX,rayDirY,rayDirZ) {
    let result = {}
    result.hit = false
    result.distance = 1e30
    result.nx = 0; result.ny = 0; result.nz = 0;

    level.entities.forEach(e => {
        if (!e.hitScanTarget) return

        let intersection = lineToPlane(
            posX, posY, posZ, 
            posX + rayDirX, posY + rayDirY, posZ + rayDirZ,     
            e.x, e.y, e.z,
            e.x - posX, e.y - posY, e.z - posZ
        )
        if (!intersection || intersection.distance <= 0) return
        if (result.distance <= intersection.distance) return
        if (!between(intersection.y, e.y - e.hitBoxTop, e.y + e.hitBoxBottom)) return
        let dxz = pointDistance2d(intersection.x, intersection.z,  e.x,e.z)
        if (!between(dxz, 0, e.hitBoxWidth/2)) return

        result = intersection
        result.hit = true
        result.entityHit = true
        result.entity = e

        let dnxz = pointDistance2d(e.x,e.z, posX,posZ)
        result.ny = 0;
        result.nx = (posX - e.x) / dnxz
        result.nz = (posZ - e.z) / dnxz
    })

    return result
}

export function doWeaponHitScan(level, posX,posY,posZ, lookDirectionHorizontal, lookDirectionVertical) {
    let vx = Math.cos(-lookDirectionHorizontal) * Math.cos(-lookDirectionVertical)
    let vz = Math.sin(-lookDirectionHorizontal) * Math.cos(-lookDirectionVertical)
    let vy = Math.sin(-lookDirectionVertical)

    let levelHitScan = doHitScanLevel(level, posX, posY, posZ, vx,vy,vz)
    let entitiesHitScan = doHitScanEntities(level, posX, posY, posZ, vx,vy,vz)
    let result = { hit: false }
    if (levelHitScan.hit) result = levelHitScan
    if (entitiesHitScan.hit && (result.hit == false || entitiesHitScan.distance < levelHitScan.distance)) result = entitiesHitScan

    if (result.levelHit) {
        result.hitColor = result.hitTile.color
    } else if (result.entityHit) {
        result.hitColor = result.entity.color
    }
    return result
}