import { keysDown, keysPressed } from '.'
import { stepTo, pushMatrices, popMatrices } from './util'
import { g3d, p } from '.'
import { drawSpriteModel } from './models'
import { TEXTURES } from './graphics'
import { playSound, SOUNDS } from './audio'
import { doWeaponHitScan } from './collisions'
import { BulletHole, BulletFrag } from "./entities/particles";
import { Enemy } from './entities/enemy'

export class Weapon {
    constructor() {
        this.clip = 30
        this.ammo = 0

        this.reloading = false
        this.reloadTimer = 0

        this.fireTimer = 0

        this.clipSize = 30
        this.maxAmmo = 300
        this.reloadDelay = 2
        this.fireDelay = 0.1
    }

    update(delta) {
        this.reloadTimer = stepTo(this.reloadTimer, 0, delta)
        this.fireTimer = stepTo(this.fireTimer, 0, delta)

        if (this.reloading && this.reloadTimer == 0) {
            this.reloading = false
            this.ammo += this.clip
            this.clip = Math.min(this.clipSize, this.ammo)
            this.ammo -= this.clip
        }
    }

    reload() {
        if (!this.reloading && this.clip < this.clipSize && this.ammo > 0) {
            playSound(SOUNDS.machine_gun_reload)
            this.reloading = true
            this.reloadTimer = this.reloadDelay
        }        
    }

    fire(player, fireButton) {
        if (keysDown[fireButton] && this.fireTimer == 0 && this.clip > 0 && !this.reloading) {
                player.recoil = 0.7
                this.fireTimer = this.fireDelay
                this.clip--
                playSound(SOUNDS.machine_gun_shoot)

                let result = doWeaponHitScan(player.level, player.x, player.y - player.radius, player.z, player.lookDirectionHorizontal, player.lookDirectionVertical)
                if (result.hit) {
                    if (result.levelHit) player.level.addEntity(new BulletHole()).setPos(result.x, result.y, result.z)
                    let e = result.entity
                    if (result.entityHit && e && e instanceof Enemy) {
                        playSound(SOUNDS.hitmarker, 0.25)
                        result.entity.damage(25)
                    }

                    for(let i = 0; i < 10; i++) {
                        player.level.addEntity(new BulletFrag(result)).setPos(
                            result.x + result.nx * 0.05, 
                            result.y + result.ny * 0.05, 
                            result.z + result.nz * 0.05
                        )                
                    }
                }
        } 
        if (keysDown[fireButton] && this.clip == 0 && !this.reloading) this.reload()
        if (keysPressed[fireButton] && this.clip == 0 && this.ammo == 0) playSound(SOUNDS.machine_gun_click)
        
    }

    draw(player) {
        pushMatrices(g3d)
            g3d.translate(player.x, player.y - player.radius + 0.01, player.z)
            g3d.rotateY(player.lookDirectionHorizontal)
            g3d.rotateZ(-player.lookDirectionVertical)
            g3d.scale(0.05)
            let y = p.lerp(0.25, 0.28, player.aimDownSights)
            if (this.reloading) y += 2 * Math.sin(Math.PI * this.reloadTimer / this.reloadDelay)
            g3d.translate(1, y, p.lerp(0.35, 0, player.aimDownSights))
            let fireAnimation = this.fireTimer / this.fireDelay
            g3d.translate(-fireAnimation * 0.1,0,0)
            g3d.rotateZ(-fireAnimation * p.radians(5))
            drawSpriteModel(TEXTURES.machine_gun)
        popMatrices(g3d)
    }

    drawHUD(player, x,y) {
        p.fill(255); p.textSize(32)
        p.textAlign(p.RIGHT, p.BOTTOM)
        p.text(this.clip + " / " + this.ammo, x,y)
    }
}

export class WeaponMachineGun extends Weapon {
    constructor() {
        super()
        this.clip = 30
        this.ammo = 180

        this.clipSize = 30
        this.maxAmmo = 300
        this.reloadDelay = 2
        this.fireDelay = 0.1
    }
}