import p5 from "p5"
import { TILES } from './tiles'
import { setFill, setNoFill, pushMatrices, popMatrices } from "./util"
import { g3d } from "."
import { theShader } from "./graphics"

export const MODELS = {}

export function createModels() {
    MODELS.RAMP = new p5.Geometry()
    MODELS.RAMP.vertices = [
        new p5.Vector(0,0,0),
        new p5.Vector(1,0,0),
        new p5.Vector(0,1,1),
        new p5.Vector(1,1,1),

        new p5.Vector(0,0,0),
        new p5.Vector(0,1,1),
        new p5.Vector(0,1,0),        

        new p5.Vector(1,0,0),
        new p5.Vector(1,1,0),
        new p5.Vector(1,1,1),
    ]
    MODELS.RAMP.faces = [
        [0,1,2],
        [2,1,3],
        [4,5,6],
        [7,8,9]
    ]
    MODELS.RAMP.uvs = [
        0,0, 1,0, 0,1, 1,1, 
        1,0, 0,1, 1,1,
        1,0, 1,1, 0,1,
    ]
    MODELS.RAMP.computeNormals()

    MODELS.BLOCK = new p5.Geometry();
    MODELS.BLOCK.gid = "uniqueName2";
    MODELS.BLOCK.vertices = [

        // Front face
        new p5.Vector( -1.0, -1.0,  1.0 ),
        new p5.Vector(  1.0, -1.0,  1.0 ),
        new p5.Vector(  1.0,  1.0,  1.0 ),
        new p5.Vector( -1.0,  1.0,  1.0 ),

        // Back face
        new p5.Vector( -1.0, -1.0, -1.0 ),
        new p5.Vector( -1.0,  1.0, -1.0 ),
        new p5.Vector(  1.0,  1.0, -1.0 ),
        new p5.Vector(  1.0, -1.0, -1.0 ),

        // Top face
        new p5.Vector( -1.0,  1.0, -1.0 ),
        new p5.Vector( -1.0,  1.0,  1.0 ),
        new p5.Vector(  1.0,  1.0,  1.0 ),
        new p5.Vector(  1.0,  1.0, -1.0 ),

        // Bottom face
        new p5.Vector( -1.0, -1.0, -1.0 ),
        new p5.Vector(  1.0, -1.0, -1.0 ),
        new p5.Vector(  1.0, -1.0,  1.0 ),
        new p5.Vector( -1.0, -1.0,  1.0 ),

        // Right face
        new p5.Vector(  1.0, -1.0, -1.0 ),
        new p5.Vector(  1.0,  1.0, -1.0 ),
        new p5.Vector(  1.0,  1.0,  1.0 ),
        new p5.Vector(  1.0, -1.0,  1.0 ),

        // Left face
        new p5.Vector( -1.0, -1.0, -1.0 ),
        new p5.Vector( -1.0, -1.0,  1.0 ),
        new p5.Vector( -1.0,  1.0,  1.0 ),
        new p5.Vector( -1.0,  1.0, -1.0 ),
    ];
    MODELS.BLOCK.faces = [

        [0,  1,  2 ],     [0,  2,  3 ],  // front
        [4,  5,  6 ],     [4,  6,  7 ],  // back
        [8,  9,  10],     [8,  10, 11],  // top
        [12, 13, 14],     [12, 14, 15],  // bottom
        [16, 17, 18],     [16, 18, 19],  // right
        [20, 21, 22],     [20, 22, 23],  // left
    ];
    MODELS.BLOCK.uvs = [

        // Front
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
        // Back
        0.0,  0.0,
        0.0,  1.0,
        1.0,  1.0,
        1.0,  0.0,
        
    
        // Top
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
        // Bottom
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
    
        // Right
        0.0,  0.0,
        0.0,  1.0,
        1.0,  1.0,
        1.0,  0.0,
        // Left
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,       
        
    ];
    MODELS.BLOCK.computeNormals()
}

export function createLevelModel(level) {
    let model = new p5.Geometry()
    for(let z = 0; z < level.zsize; z++)
    for(let y = 0; y < level.ysize; y++)
    for(let x = 0; x < level.xsize; x++) {
        let t = level.getTile(x,y,z)
        if (t == TILES.EMPTY) continue

        let vi = model.vertices.length
        let fi = model.faces.length
        let b = 0.002
        let u0 = t.u0 + b, u1 = t.u1 - b
        let v0 = t.v0 + b, v1 = t.v1 - b
        
        if (t.type.solid_block) {
            model.vertices.push(
            // Front face
            new p5.Vector( -1.0, -1.0,  1.0 ),
            new p5.Vector(  1.0, -1.0,  1.0 ),
            new p5.Vector(  1.0,  1.0,  1.0 ),
            new p5.Vector( -1.0,  1.0,  1.0 ),

            // Back face
            new p5.Vector( -1.0, -1.0, -1.0 ),
            new p5.Vector( -1.0,  1.0, -1.0 ),
            new p5.Vector(  1.0,  1.0, -1.0 ),
            new p5.Vector(  1.0, -1.0, -1.0 ),

            // Top face
            new p5.Vector( -1.0,  1.0, -1.0 ),
            new p5.Vector( -1.0,  1.0,  1.0 ),
            new p5.Vector(  1.0,  1.0,  1.0 ),
            new p5.Vector(  1.0,  1.0, -1.0 ),

            // Bottom face
            new p5.Vector( -1.0, -1.0, -1.0 ),
            new p5.Vector(  1.0, -1.0, -1.0 ),
            new p5.Vector(  1.0, -1.0,  1.0 ),
            new p5.Vector( -1.0, -1.0,  1.0 ),

            // Right face
            new p5.Vector(  1.0, -1.0, -1.0 ),
            new p5.Vector(  1.0,  1.0, -1.0 ),
            new p5.Vector(  1.0,  1.0,  1.0 ),
            new p5.Vector(  1.0, -1.0,  1.0 ),

            // Left face
            new p5.Vector( -1.0, -1.0, -1.0 ),
            new p5.Vector( -1.0, -1.0,  1.0 ),
            new p5.Vector( -1.0,  1.0,  1.0 ),
            new p5.Vector( -1.0,  1.0, -1.0 ),
            )
            model.vertexNormals.push(
                // Front face
                new p5.Vector( 0.0, 0.0, 1.0), new p5.Vector( 0.0, 0.0, 1.0), new p5.Vector( 0.0, 0.0, 1.0), new p5.Vector( 0.0, 0.0, 1.0),
                // Back face
                new p5.Vector( 0.0, 0.0, -1.0), new p5.Vector( 0.0, 0.0, -1.0), new p5.Vector( 0.0, 0.0, -1.0), new p5.Vector( 0.0, 0.0, -1.0),
                // Top face
                new p5.Vector( 0.0, 1.0, 0.0), new p5.Vector( 0.0, 1.0, 0.0), new p5.Vector( 0.0, 1.0, 0.0), new p5.Vector( 0.0, 1.0, 0.0), 
                // Bottom face
                new p5.Vector( 0.0, -1.0, 0.0 ), new p5.Vector( 0.0, -1.0, 0.0 ), new p5.Vector( 0.0, -1.0, 0.0 ), new p5.Vector( 0.0, -1.0, 0.0 ),    
                // Right face
                new p5.Vector( 1.0, 0.0, 0.0 ), new p5.Vector( 1.0, 0.0, 0.0 ), new p5.Vector( 1.0, 0.0, 0.0 ), new p5.Vector( 1.0, 0.0, 0.0 ),    
                // Left face
                new p5.Vector( -1.0, 0.0, 0.0 ), new p5.Vector( -1.0, 0.0, 0.0 ), new p5.Vector( -1.0, 0.0, 0.0 ), new p5.Vector( -1.0, 0.0, 0.0 ),
            )
            model.faces.push(
                [0,  1,  2 ],     [0,  2,  3 ],  // front
                [4,  5,  6 ],     [4,  6,  7 ],  // back
                [8,  9,  10],     [8,  10, 11],  // top
                [12, 13, 14],     [12, 14, 15],  // bottom
                [16, 17, 18],     [16, 18, 19],  // right
                [20, 21, 22],     [20, 22, 23],  // left
            )
            model.uvs.push(
                // Front
                u0, v0,
                u1, v0,
                u1, v1,
                u0, v1,
                // Back
                u0, v0,
                u0, v1,
                u1, v1,
                u1, v0,
                
                // Top
                u0, v0,
                u1, v0,
                u1, v1,
                u0, v1,
                // Bottom
                u0, v0,
                u1, v0,
                u1, v1,
                u0, v1,
            
                // Right
                u0, v0,
                u0, v1,
                u1, v1,
                u1, v0,
                // Left
                u0, v0,
                u1, v0,
                u1, v1,
                u0, v1,  
            )

            for(let i = vi; i < model.vertices.length; i++) {
                model.vertices[i].x = model.vertices[i].x / 2 + 0.5 + x
                model.vertices[i].y = model.vertices[i].y / 2 + 0.5 + y
                model.vertices[i].z = model.vertices[i].z / 2 + 0.5 + z
            }
            for(let i = fi; i < model.faces.length; i++) {
                model.faces[i][0] += vi
                model.faces[i][1] += vi
                model.faces[i][2] += vi
            }
        } else if (t.type.ramp) {
            model.vertices.push(
                new p5.Vector(0,0,0),
                new p5.Vector(1,0,0),
                new p5.Vector(0,1,1),
                new p5.Vector(1,1,1),
        
                new p5.Vector(0,0,0),
                new p5.Vector(0,1,1),
                new p5.Vector(0,1,0),        
        
                new p5.Vector(1,0,0),
                new p5.Vector(1,1,0),
                new p5.Vector(1,1,1),
            )
            model.vertexNormals.push(
                new p5.Vector(0, -Math.SQRT2, Math.SQRT2),
                new p5.Vector(0, -Math.SQRT2, Math.SQRT2),
                new p5.Vector(0, -Math.SQRT2, Math.SQRT2),
                new p5.Vector(0, -Math.SQRT2, Math.SQRT2),
        
                new p5.Vector(-1,0,0),
                new p5.Vector(-1,0,0),
                new p5.Vector(-1,0,0),        
        
                new p5.Vector(1,0,0),
                new p5.Vector(1,0,0),
                new p5.Vector(1,0,0),
            )
            model.faces.push(
                [0,1,2],
                [2,1,3],
                [4,5,6],
                [7,8,9]
            )
            model.uvs.push(
                u0,v0, u1,v0, u0,v1, u1,v1, 
                u1,v0, u0,v1, u1,v1,
                u1,v0, u1,v1, u0,v1,
            )

            let a = t.model_rotation_y
            for(let i = vi; i < model.vertices.length; i++) {
                model.vertices[i].x -= 0.5
                model.vertices[i].z -= 0.5
                let rx = model.vertices[i].x * Math.cos(a) + model.vertices[i].z * Math.sin(a)
                let rz = -model.vertices[i].x * Math.sin(a) + model.vertices[i].z * Math.cos(a)

                model.vertices[i].x = rx + 0.5 + x
                model.vertices[i].z = rz + 0.5 + z
                model.vertices[i].y += y
            }
            for(let i = fi; i < model.faces.length; i++) {
                model.faces[i][0] += vi
                model.faces[i][1] += vi
                model.faces[i][2] += vi
            }
        }

       
    }
    console.log('v:', model.vertices.length, 'f:', model.faces.length, 'n:', model.vertexNormals.length)
    return model;
}

export function drawSpriteModel(texture) {
    pushMatrices(g3d)
    g3d.translate(-0.5, -0.5)
    g3d.scale(1 / 16)
    for(let y = 0; y < 16; y++)
    for(let x = 0; x < 16; x++) {
        let i = 4 * (16 * y + x)
        if (texture.pixels[i + 3] != 0) {
            pushMatrices(g3d)
            g3d.translate(x,y,0)
            setFill([texture.pixels[i] / 255, texture.pixels[i + 1] / 255, texture.pixels[i + 2] / 255, 1.0])
            g3d.box(1)
            popMatrices(g3d)
        }
    }
    setNoFill(theShader)
    popMatrices(g3d)
    
}