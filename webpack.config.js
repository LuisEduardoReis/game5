const path = require('path');
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
    mode: "development",
    entry: './src/index.js',
    module: {
        rules: [
        ],
    },
    plugins: [
        new CopyPlugin({
            patterns: [
                { from: 'src/shaders', to: './shaders'},
                { from: 'src/assets', to: './assets'},
                { from: 'index.html', to: '.'},
            ]
        })
    ],
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist'),
    },
    devServer: {
        static: './dist',
    }
};